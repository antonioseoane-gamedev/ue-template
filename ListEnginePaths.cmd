@echo off
setlocal

set VALUE_NAME=InstalledDirectory
set KEY_NAME="HKEY_LOCAL_MACHINE\SOFTWARE\EpicGames\Unreal Engine"

for /f "usebackq tokens=*" %%A in (`reg query %KEY_NAME%  /reg:64`) do (
    for /f "tokens=5 delims=\" %%b in ("%%A") do (
        call :FVersion %%b
    )
    for /f "usebackq tokens=1,2,*" %%A in (`reg query "%%A" /reg:64`) do (
        if "%%A" == "%VALUE_NAME%" (
            call :FVersionPath %%C
        )
    )
)

goto End

:FVersion
set VERSION=%1
goto :eof

:FVersionPath
set UE_PATH=%1
echo Engine version: ^<%VERSION%^> - Path: ^<%UE_PATH%^>
goto :eof

:End